import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  constructor(public platform: Platform, public navCtrl: NavController, private iab: InAppBrowser) {
    // window.open = InAppBrowser.open;
  }

  ngOnInit(){
      const browser = this.iab.create('https://www.alltim.es/','_self',{location:'no'});
  }

}
